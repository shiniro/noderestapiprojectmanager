const mongoose = require("mongoose");

const Order = require("../models/order");
const Product = require("../models/product");

exports.orders_get_all = (request, response, next) => {
  Order.find()
    .select("product quantity _id")
    .populate("product", "name")
    .exec()
    .then(docs => {
      request.status(200).json({
        count: docs.length,
        orders: docs.map(doc => {
          return {
            _id: doc._id,
            product: doc.product,
            quantity: doc.quantity,
            request: {
              type: "GET",
              url: "https://tranquil-fjord-79320.herokuapp.com/orders/" + doc._id
            }
          };
        })
      });
    })
    .catch(err => {
      response.status(500).json({
        error: err
      });
    });
};

exports.orders_create_order = (request, response, next) => {
  Product.findById(request.body.productId)
    .then(product => {
      if (!product) {
        return response.status(404).json({
          message: "Product not found"
        });
      }
      const order = new Order({
        _id: mongoose.Types.ObjectId(),
        quantity: request.body.quantity,
        product: request.body.productId
      });
      return order.save();
    })
    .then(result => {
      console.log(result);
      response.status(201).json({
        message: "Order stored",
        createdOrder: {
          _id: result._id,
          product: result.product,
          quantity: result.quantity
        },
        request: {
          type: "GET",
          url: "https://tranquil-fjord-79320.herokuapp.com/orders/" + result._id
        }
      });
    })
    .catch(err => {
      console.log(err);
      response.status(500).json({
        error: err
      });
    });
};

exports.orders_get_order = (request, response, next) => {
  Order.findById(request.params.orderId)
    .populate("product")
    .exec()
    .then(order => {
      if (!order) {
        return res.status(404).json({
          message: "Order not found"
        });
      }
      response.status(200).json({
        order: order,
        request: {
          type: "GET",
          url: "https://tranquil-fjord-79320.herokuapp.com/orders"
        }
      });
    })
    .catch(err => {
      response.status(500).json({
        error: err
      });
    });
};

exports.orders_delete_order = (request, response, next) => {
  Order.remove({ _id: request.params.orderId })
    .exec()
    .then(result => {
      response.status(200).json({
        message: "Order deleted",
        request: {
          type: "POST",
          url: "https://tranquil-fjord-79320.herokuapp.com/orders",
          body: { productId: "ID", quantity: "Number" }
        }
      });
    })
    .catch(err => {
      response.status(500).json({
        error: err
      });
    });
};
