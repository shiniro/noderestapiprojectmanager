const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');

exports.user_signup = (request, response, next) => {
  User.find({ email: request.body.email })
    .exec()
    .then(user => {
      if (user.length >= 1) {
        return response.status(409).json({
          message: 'Email already used'
        }) // 409 = conflict - 422 = unproccessable entity
      } else {
        bcrypt.hash(request.body.password, 10, (error, hash) => {
          if (error) {
            return response.status(500).json({
              error: error
            });
          } else {
            const user = new User({
              _id: new mongoose.Types.ObjectId(),
              email: request.body.email,
              password: hash
            });
            user.save()
              .then(result => {
                console.log(result);
                response.status(201).json({
                  message: 'User created',
                  user: result
                });
              })
              .catch(error => {
                console.log(error);
                response.status(500).json({
                  error: error
                });
              });
          }
        }); // 10 random number for more security
      }
    })
};

exports.user_login = (request, response, next) => {
  User.find({ email: request.body.email }) // can use findOne to get just one user
    .exec()
    .then(user => {
      if (user.length < 1) {
        return response.status(401).json({
          messages: 'Authentification failed'
        }); // 401 => got no email or password wrong - authentification failed
      } else {
        bcrypt.compare(request.body.password, user[0].password, (error, result) => {
          if (error) {
            return response.status(401).json({
              messages: 'Authentification failed - email incorrect'
            });
          } 
          if (result) {
            const token = jwt.sign(
              {
                email: user[0].email,
                userId: user[0]._id
              }, 
              process.env.JWT_KEY,
              {
                expiresIn: "1h"
              }
            );
            return response.status(200).json({
              messages: 'Authentification successful',
              token: token
            });
          }
          return response.status(401).json({
            messages: 'Authentification failed - password incorrect'
          });
        });
      }
    })
    .catch( error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};

exports.user_delete_user = (request, response, next) => {
  User.remove({ _id: request.params.userId })
    .exec()
    .then(result => {
      response.status(200).json({
        message: "User deleted"
      })
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};