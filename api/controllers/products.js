const mongoose = require('mongoose');

const Product = require('../models/product');

exports.products_get_all = (request, response, next) => {
  Product.find()
    .select("name price _id productImage")
    .exec()
    .then(docs => {
      console.log(docs);
      const res = {
        count: docs.length,
        products: docs.map( doc => {
          return {
            id: doc._id,
            name: doc.name,
            price: doc.price,
            productImage: doc.productImage,
            request: {
              type: 'GET',
              url: 'https://tranquil-fjord-79320.herokuapp.com/products/' + doc._id
            }
          }
        })
      }
      // if (docs.length >= 0) {
        response.status(200).json(res);
      /* } else {
        response.status(404).json({
          message: 'No entries found'
        });
      } */
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    })
};

exports.products_create_product = (request, response, next) => { //single -. one file only
  console.log(request.file);
  const product = new Product({
    _id: new mongoose.Types.ObjectId(),
    name: request.body.name,
    price: request.body.price,
    productImage: request.file.path
  });
  product.save().then(result => {
    console.log(result);
    response.status(201).json ({
      message: 'Created product successfully',
      createdProduct: {
        name: result.name,
        price: result.price,
        id: result._id,
        productImage: result.productImage,
        request: {
          type: 'POST',
          url: 'https://tranquil-fjord-79320.herokuapp.com/products/' + result._id
        }
      }
    });
  }).catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};

exports.products_get_product = (request, response, next) => {
  const productId = request.params.productId;
  Product.findById(productId)
  .select('name price _id productImage')
  .exec()
  .then(doc => {
    console.log("From database: ", doc);
    if (doc) {
      response.status(200).json({
        product: doc,
        request: {
          type: 'GET',
          url: 'https://tranquil-fjord-79320.herokuapp.com/products/' + doc._id
        }
      });
    } else {
      response.status(404).json({
        message: 'No valid entry found for provided ID'
      });
    }
  })
  .catch(error => {
    console.log(error);
    response.status(500).json({error: error});
  });
};

exports.products_update_product = (request, response, next) => {
  const id = request.params.productId;
  const updateOperations = {};
  for (const ops of request.body) {
    updateOperations[ops.propName] = ops.value;
  }
  Product.update({_id:id}, { $set: updateOperations })
    .exec()
    .then(result => {
      console.log(result);
      response.status(200).json({
        message: 'Product updated',
        request: {
          type: 'GET',
          url: 'https://tranquil-fjord-79320.herokuapp.com/products/' + id
        }
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({error: error});
    });
};

exports.products_delete_product = (request, response, next) => {
  const id = request.params.productId
  Product.remove({_id:id})
    .exec()
    .then(result => {
      response.status(200).json({
        message: 'Product deleted',
        product: result
      });
    })
    .catch(error => {
      console.log(error);
      response.status(500).json({
        error: error
      });
    });
};